$(function(){
	
})


function search() {
	$("input[name='pageNum']").val(1);
	$("#searchForm").submit();
}

function add() {
	$("#id").val(""); 
	$("#name").val("");
	$("input[name='userIds']").prop("checked", false);
	$("#status option:first").prop("checked", true);
	form.render();
	showWindow("添加项目");
}


function showWindow(title){
	layer.open({
		type : 1,
		title : title,
		area : [ '600px', '400px' ], // 宽高
		content : $('#windowDiv')
	});
}

function addOver() {
	if ($("#name").val() == "") {
		layer.msg("项目名为空");
		return;
	}
	var hasUser = false;
	$("input[name='userIds']").each(function() {
		if($(this).prop("checked")){
			hasUser = true;
		}
	});

	if(!hasUser){
		layer.msg("参与者为空");
		return;
	}
	showLoad();
	$.ajax({
		type : 'POST',
		url : ctx + '/adminPage/project/addOver',
		data : $('#addForm').serialize(),
		dataType : 'json',
		success : function(data) {
			closeLoad();
			if (data.success) {
				location.reload();
			} else {
				layer.msg(data.msg);
			}
		},
		error : function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});
	
	
}

function edit(id) {
	layer.load();
	$.ajax({
		type : 'GET',
		url : ctx + '/adminPage/project/detail',
		dataType : 'json',
		data : {
			id : id
		},
		success : function(data) {
			closeLoad();
			if (data.success) {
				var project = data.obj;
				$("#id").val(project.id); 
				$("#name").val(project.name);
				
				$("input[name='userIds']").prop("checked", false);
				for(var i=0;i<project.userList.length;i++){
					var user = project.userList[i];
					$("#users_" + user.id).prop("checked", true);
				}
				$("#status").val(project.status);
				
				form.render();
				showWindow("编辑项目");
			}else{
				layer.msg(data.msg);
			}
		},
		error : function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});
}

function del(id){
	if(confirm("确认删除?")){
		layer.load();
		$.ajax({
			type : 'POST',
			url : ctx + '/adminPage/project/del',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(data) {
				closeLoad();
				if (data.success) {
					location.reload();
				}else{
					layer.msg(data.msg)
				}
			},
			error : function() {
				closeLoad();
				alert("出错了,请联系技术人员!");
			}
		});
	}
}


