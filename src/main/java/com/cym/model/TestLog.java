package com.cym.model;

import com.cym.sqlhelper.bean.BaseModel;
import com.cym.sqlhelper.config.Table;

//测试日志
@Table
public class TestLog extends BaseModel {

	String functionId;

	// 操作人
	String userId;

	// 切换到状态 0激活 1已修改 2已验证 3已否决 4已搁置
	Integer toTestStatus;

	// 指派到人,用逗号隔开
	String toUserIds;

	// 切换时间
	Long changeTime;

	// 切换备注
	String changeMark;

	public String getToUserIds() {
		return toUserIds;
	}

	public void setToUserIds(String toUserIds) {
		this.toUserIds = toUserIds;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public Integer getToTestStatus() {
		return toTestStatus;
	}

	public void setToTestStatus(Integer toTestStatus) {
		this.toTestStatus = toTestStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Long changeTime) {
		this.changeTime = changeTime;
	}

	public String getChangeMark() {
		return changeMark;
	}

	public void setChangeMark(String changeMark) {
		this.changeMark = changeMark;
	}

}
