package com.cym.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Init;
import org.noear.solon.annotation.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cym.model.User;
import com.cym.service.UserService;
import com.cym.sqlhelper.utils.SqlHelper;
import com.cym.utils.FilePermissionUtil;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ClassPathResource;

@Component
public class InitConfig {
	Logger logger = LoggerFactory.getLogger(InitConfig.class);
	@Inject
	HomeConfig homeConfig;
	@Inject
	VersionConfig versionConfig;
	@Inject("${project.findPass}")
	Boolean findPass;
	@Inject
	SqlHelper sqlHelper;
	@Inject
	UserService userService;

	@Init
	public void init() {
		// 打印密码
		if (findPass) {
			List<User> users = sqlHelper.findAll(User.class);
			for (User user : users) {
				System.out.println("用户名:" + user.getName() + " 密码:" + user.getPass());
			}
			System.exit(1);
		}
		
		// 建立管理员账户
		List<User> users = sqlHelper.findAll(User.class);
		if(users.size() == 0) {
			User user = new User();
			user.setName("admin");
			user.setPass("admin");
			user.setType(0);
			user.setPermission(1);
			user.setTrueName("管理员");
			
			sqlHelper.insert(user);
		}

		// 检查目录读写权限
		if (!FilePermissionUtil.canWrite(new File(homeConfig.home))) {
			logger.error(homeConfig.home + " " + "目录没有可写权限,请重新指定.");
			return;
		}

		// 创建文件目录
		FileUtil.mkdir(homeConfig.home + "file");

		// 展示logo
		showLogo();

	}

	/**
	 * 显示logo
	 * 
	 * @throws IOException
	 */
	private void showLogo() {
		try {
			ClassPathResource resource = new ClassPathResource("banner.txt");
			BufferedReader reader = resource.getReader(Charset.forName("utf-8"));
			String str = null;
			StringBuilder stringBuilder = new StringBuilder();
			// 使用readLine() 比较方便的读取一行
			while (null != (str = reader.readLine())) {
				stringBuilder.append(str + "\n");
			}
			reader.close();// 关闭流

			stringBuilder.append("weekReport " + versionConfig.currentVersion + "\n");

			logger.info(stringBuilder.toString());
		} catch (IOException e) {
			logger.info(e.getMessage(), e);
		}
	}
}
