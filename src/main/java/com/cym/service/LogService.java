package com.cym.service;

import java.util.List;

import org.noear.solon.annotation.Inject;
import org.noear.solon.aspect.annotation.Service;

import com.cym.model.Function;
import com.cym.model.FunctionLog;
import com.cym.model.FunctionUser;
import com.cym.sqlhelper.utils.ConditionAndWrapper;
import com.cym.sqlhelper.utils.SqlHelper;

import cn.hutool.core.util.StrUtil;

@Service
public class LogService {
	@Inject
	SqlHelper sqlHelper;

//	public List<FunctionLog> getPersonLog(String startTime, String endTime, String userId, Integer status, Integer type) {
//
//		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper();
//		
//		if (type != null) {
//			ConditionAndWrapper functionWrapper = new ConditionAndWrapper();
//			functionWrapper.eq(Function::getType, type);
//			List<String> functionIds = sqlHelper.findPropertiesByQuery(functionWrapper, Function.class, Function::getId);
//			conditionAndWrapper.in(FunctionLog::getFunctionId, functionIds);
//		}
//		if (StrUtil.isNotEmpty(userId)) {
//			conditionAndWrapper.eq(FunctionLog::getUserId, userId);
//		}
//		if (StrUtil.isNotEmpty(startTime)) {
//			conditionAndWrapper.gte(FunctionLog::getTime, startTime);
//		}
//		if (StrUtil.isNotEmpty(endTime)) {
//			conditionAndWrapper.lte(FunctionLog::getTime, endTime);
//		}
//		if (status != null) {
//			conditionAndWrapper.eq(FunctionLog::getToStatus, status);
//		}
//
//		return sqlHelper.findListByQuery(conditionAndWrapper, FunctionLog.class);
//	}

	public List<FunctionLog> getProjectLog(Long startTime, Long endTime, String projectId, Integer status, String userId, Integer type) {

		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper();

		if (type != null) {
			ConditionAndWrapper functionWrapper = new ConditionAndWrapper();
			functionWrapper.eq(Function::getType, type);
			List<String> functionIds = sqlHelper.findPropertiesByQuery(functionWrapper, Function.class, Function::getId);
			conditionAndWrapper.in(FunctionLog::getFunctionId, functionIds);
		}

		if (startTime != null) {
			conditionAndWrapper.gte(FunctionLog::getTime, startTime);
		}
		if (endTime != null) {
			conditionAndWrapper.lte(FunctionLog::getTime, endTime);
		}
		if (StrUtil.isNotEmpty(userId)) {
			ConditionAndWrapper userWrapper = new ConditionAndWrapper();
			userWrapper.eq(FunctionUser::getUserId, userId);
			List<String> functionIds = sqlHelper.findPropertiesByQuery(userWrapper, FunctionUser.class, FunctionUser::getFunctionId);
			conditionAndWrapper.in(FunctionLog::getFunctionId, functionIds);
		}
		if (status != null) {
			conditionAndWrapper.eq(FunctionLog::getToStatus, status);
		}
		return sqlHelper.findListByQuery(conditionAndWrapper, FunctionLog.class);
	}

}
