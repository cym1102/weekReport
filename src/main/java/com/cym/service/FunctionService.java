package com.cym.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.noear.solon.annotation.Inject;
import org.noear.solon.aspect.annotation.Service;

import com.cym.model.Function;
import com.cym.model.FunctionLog;
import com.cym.model.FunctionUser;
import com.cym.model.TestLog;
import com.cym.model.User;
import com.cym.sqlhelper.bean.Page;
import com.cym.sqlhelper.bean.Sort;
import com.cym.sqlhelper.bean.Sort.Direction;
import com.cym.sqlhelper.utils.ConditionAndWrapper;
import com.cym.sqlhelper.utils.SqlHelper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class FunctionService {
	@Inject
	SqlHelper sqlHelper;

	public Page<Function> search(Page pageReq, String userId, String startTime, String endTime, Integer status, String projectId, Integer level, Integer type,
			String createUserId) {
		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper().eq(Function::getProjectId, projectId);
		if (StrUtil.isNotEmpty(userId)) {
			List<String> functionIds = sqlHelper.findPropertiesByQuery(new ConditionAndWrapper().eq(FunctionUser::getUserId, userId), FunctionUser.class, FunctionUser::getFunctionId);
			conditionAndWrapper.in(Function::getId, functionIds);
		}
		if (StrUtil.isNotEmpty(createUserId)) {
			conditionAndWrapper.eq(Function::getCreateUserId, createUserId);
		}
		if (StrUtil.isNotEmpty(startTime)) {
			conditionAndWrapper.gte(Function::getEndTime, startTime);
		}
		if (StrUtil.isNotEmpty(endTime)) {
			conditionAndWrapper.lte(Function::getEndTime, endTime);
		}
		if (level != null) {
			conditionAndWrapper.eq(Function::getLevel, level);
		}
		if (type != null) {
			conditionAndWrapper.eq(Function::getType, type);
		}
//		if (testStatus != null) {
//			conditionAndWrapper.eq(Function::getTestStatus, testStatus);
//		}
		conditionAndWrapper.eq(Function::getStatus, status);

		Sort sort = new Sort();
//		sort.add(Function::getMoveTime, Direction.DESC);
		sort.add(Function::getId, Direction.DESC);
		return sqlHelper.findPage(conditionAndWrapper, sort, pageReq, Function.class);
	}

	public List<User> getUserList(String functionId) {
		List<String> userIds = sqlHelper.findPropertiesByQuery(new ConditionAndWrapper().eq(FunctionUser::getFunctionId, functionId), FunctionUser.class, FunctionUser::getUserId);

		return sqlHelper.findListByIds(userIds, User.class);
	}

//	public List<Function> getByProjectIdStatus(String projectId, Integer status, String functionEndTime) {
//		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper().eq(Function::getProjectId, projectId).eq(Function::getStatus, status);
//		if (StrUtil.isNotEmpty(functionEndTime)) {
//			conditionAndWrapper.eq(Function::getEndTime, functionEndTime);
//		}
//
//		return sqlHelper.findListByQuery(conditionAndWrapper, Function.class);
//	}

	public void insertOrUpdate(Function function, String[] userIds, String loginUserId) {
		boolean isAdd = StrUtil.isEmpty(function.getId());

		sqlHelper.insertOrUpdate(function);

		sqlHelper.deleteByQuery(new ConditionAndWrapper().eq(FunctionUser::getFunctionId, function.getId()), FunctionUser.class);

		for (String userId : userIds) {
			FunctionUser functionUser = new FunctionUser();
			functionUser.setFunctionId(function.getId());
			functionUser.setUserId(userId);
			sqlHelper.insertOrUpdate(functionUser);
		}

		// 添加, 需要计入日志
		if (isAdd) {
			FunctionLog functionLog = new FunctionLog();
			functionLog.setFunctionId(function.getId());
			functionLog.setUserId(loginUserId);
			functionLog.setToStatus(function.getStatus());
			functionLog.setTime(System.currentTimeMillis());
			sqlHelper.insert(functionLog);
		}

	}

	public void move(String functionId, Integer status, String loginUserId) {
		Function function = new Function();

		function.setId(functionId);
		function.setStatus(status);
		function.setMoveTime(System.currentTimeMillis());
		sqlHelper.updateById(function);

		// 计入日志
		FunctionLog functionLog = new FunctionLog();
		functionLog.setFunctionId(functionId);
		functionLog.setUserId(loginUserId);
		functionLog.setToStatus(status);
		functionLog.setTime(System.currentTimeMillis());
		sqlHelper.insert(functionLog);

	}

	public String getStatusName(Integer status) {
		// 0：等待阶段。1：开发阶段。2：测试阶段。3：产品确认。4任务完成. 5任务否决
		switch (status) {
		case 0:
			return "等待阶段";
		case 1:
			return "开发阶段";
		case 2:
			return "测试阶段";
		case 3:
			return "完成阶段";
		case 4:
			return "否决阶段";
		}
		return "未知";
	}

	public String getLevelName(Integer level) {
		// 0低级 1中级 2紧急 3特急
		switch (level) {
		case 0:
			return "低级";
		case 1:
			return "中级";
		case 2:
			return "紧急";
		case 3:
			return "特急";
		}
		return "未知";
	}

//	public String getTestStatusName(Integer testStatus) {
//		// 测试状态 0激活 1已修改 2已验证 3已否决
//		switch (testStatus) {
//		case 0:
//			return "激活";
//		case 1:
//			return "已修改";
//		case 2:
//			return "已验证";
//		case 3:
//			return "已否决";
//		case 4:
//			return "已搁置";
//		}
//		return "未知";
//	}

	public void delete(String id) {
		sqlHelper.deleteById(id, Function.class);
		sqlHelper.deleteByQuery(new ConditionAndWrapper().eq(FunctionLog::getFunctionId, id), FunctionLog.class);
		sqlHelper.deleteByQuery(new ConditionAndWrapper().eq(FunctionUser::getFunctionId, id), FunctionUser.class);
	}

	public void test(String functionId, String[] userIds, Integer testStatus, String userId, String changeMark) {
		Function function = sqlHelper.findById(functionId, Function.class);
		sqlHelper.deleteByQuery(new ConditionAndWrapper().eq(FunctionUser::getFunctionId, function.getId()), FunctionUser.class);

		for (String uid : userIds) {
			FunctionUser functionUser = new FunctionUser();
			functionUser.setFunctionId(function.getId());
			functionUser.setUserId(uid);
			sqlHelper.insertOrUpdate(functionUser);
		}

		TestLog testLog = new TestLog();

		testLog.setFunctionId(functionId);
		testLog.setChangeTime(System.currentTimeMillis());
		testLog.setToTestStatus(testStatus);
		testLog.setToUserIds(StrUtil.join(",", Arrays.asList(userIds)));
		testLog.setUserId(userId);
		testLog.setChangeMark(changeMark);
		sqlHelper.insert(testLog);

	}

	public List<TestLog> getTestLogList(String functionId) {

		return sqlHelper.findListByQuery(new ConditionAndWrapper().eq(TestLog::getFunctionId, functionId), new Sort(TestLog::getId, Direction.ASC), TestLog.class);
	}

}
