package com.cym.model;

import com.cym.sqlhelper.bean.BaseModel;
import com.cym.sqlhelper.config.InitValue;
import com.cym.sqlhelper.config.Table;

/**
 * 用户
 * 
 * @author 陈钇蒙
 *
 */
@Table
public class User extends BaseModel {

	// 登录名
	String name;
	// 密码
	String pass;

	// 类型 0：产品 1：开发 2：测试
	@InitValue("0")
	Integer type;

	// 权限 0:普通用戶 1:管理員
	@InitValue("0")
	Integer permission;

	// 姓名
	String trueName;
	
	// 权限 0:正常 1:禁用
	@InitValue("0")
	Integer status;
	

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public Integer getPermission() {
		return permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
