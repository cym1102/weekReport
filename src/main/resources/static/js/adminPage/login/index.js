$(function() {

	layer.open({
		type: 1,
		shade: false,
		title: $("title").html(),
		closeBtn: false,
		area: ['400px', '330px'], //宽高
		content: $('#windowDiv')
	});

})



function loginPage() {

	layer.closeAll();
	layer.open({
		type: 1,
		shade: false,
		title: $("title").html(),
		closeBtn: false,
		area: ['400px', '330px'], //宽高
		content: $('#windowDiv')
	});
}

function login() {
	showLoad();
	$.ajax({
		type: 'POST',
		url: ctx + '/adminPage/login/login',
		data: $("#loginForm").serialize(),
		dataType: 'json',
		success: function(data) {
			closeLoad();
			if (data.success) {
				if ($("#projectId").val() == 'system') {
					location.href = ctx + "adminPage/user";
				} else {
					location.href = ctx + "adminPage/function?functionStatus=0";
				}

			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});
}



function getKey() {
	if (event.keyCode == 13) {
		login();
	}
}