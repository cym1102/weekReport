package com.cym.service;

import java.util.ArrayList;
import java.util.List;

import org.noear.solon.annotation.Inject;
import org.noear.solon.aspect.annotation.Service;

import com.cym.model.Function;
import com.cym.model.FunctionUser;
import com.cym.model.ProjectUser;
import com.cym.model.User;
import com.cym.sqlhelper.bean.Page;
import com.cym.sqlhelper.utils.ConditionAndWrapper;
import com.cym.sqlhelper.utils.ConditionOrWrapper;
import com.cym.sqlhelper.utils.SqlHelper;

import cn.hutool.core.util.StrUtil;

@Service
public class UserService {
	@Inject
	SqlHelper sqlHelper;

	public User login(String name, String pass) {
		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper().eq("name", name).eq("pass", pass);

		return sqlHelper.findOneByQuery(conditionAndWrapper, User.class);
	}

	public Page<User> search(Page pageReq, String keywords) {
		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper();

		if (StrUtil.isNotEmpty(keywords)) {
			conditionAndWrapper.and(new ConditionOrWrapper().like("name", keywords));
		}

		Page<User> pageResp = sqlHelper.findPage(conditionAndWrapper, pageReq, User.class);

		return pageResp;
	}

	public User getByName(String name, String userId) {
		ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper().eq(User::getName, name);
		if (StrUtil.isNotEmpty(userId)) {
			conditionAndWrapper.ne(User::getId, userId);
		}

		return sqlHelper.findOneByQuery(conditionAndWrapper, User.class);
	}

	public void deleteById(String id) {
		sqlHelper.deleteById(id, User.class);
		sqlHelper.deleteByQuery(new ConditionAndWrapper().eq(ProjectUser::getUserId, id), ProjectUser.class);
	}

	public boolean hasUserFunction(String id) {
		return sqlHelper.findCountByQuery(new ConditionAndWrapper().eq(FunctionUser::getUserId, id), FunctionUser.class) > 0;
	}

	public String getTypeName(Integer type) {
		switch (type) {
		case 0:
			return "产品";
		case 1:
			return "开发";
		case 2:
			return "测试";
		}
		return "未知";
	}

	public boolean hasProject(String userId, String projectId) {
		if (!projectId.equals("system")) {
			Long count = sqlHelper.findCountByQuery(new ConditionAndWrapper().eq(ProjectUser::getProjectId, projectId).eq(ProjectUser::getUserId, userId), ProjectUser.class);

			return count > 0;
		} else {
			User user = sqlHelper.findById(userId, User.class);
			return user.getType() == 0;
		}
	}

	public List<User> getByProjectId(String projectId) {
		List<String> userIds = sqlHelper.findPropertiesByQuery(new ConditionAndWrapper().eq(ProjectUser::getProjectId, projectId), ProjectUser.class, ProjectUser::getUserId);
		return sqlHelper.findListByQuery(new ConditionAndWrapper().in(User::getId, userIds), User.class);
	}

	public String getUserNames(String toUserIds) {
		List<User> users = sqlHelper.findListByQuery(new ConditionAndWrapper().in(User::getId, toUserIds.split(",")), User.class);

		List<String> rs = new ArrayList<>();
		for (User user : users) {
			rs.add("【" + user.getTrueName() + "(" + getTypeName(user.getType()) + ")" + "】");
		}

		return StrUtil.join(",", rs);
	}

	public Long getStatusCount(String userId, Integer status) {

		User user = sqlHelper.findById(userId, User.class);
		Long count = 0l;

		// 开发
		if (user.getType() == 1) {
			ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper().eq(Function::getStatus, status);
			List<String> ids = sqlHelper.findPropertiesByQuery(new ConditionAndWrapper().eq(FunctionUser::getUserId, userId), FunctionUser.class, FunctionUser::getFunctionId);
			conditionAndWrapper.in(Function::getId, ids);
			// 开发阶段
			count = sqlHelper.findCountByQuery(conditionAndWrapper, Function.class);
		}

		// 测试
		if (user.getType() == 2) {
			ConditionAndWrapper conditionAndWrapper = new ConditionAndWrapper().eq(Function::getStatus, status);
			count = sqlHelper.findCountByQuery(conditionAndWrapper, Function.class);
		}

		return count;
	}

}
