package com.cym.utils;

public class MyStringUtils {
	public static String converTextToHtml(String str) {
		if (str == null) {
			return null;
		}
		return str.replace("\n", "<br>").replace("\n", "<br>").replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;").replace("\\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
	}

	public static String stripHtml(String content) {
		if (content == null) {
			return null;
		}

		// <p>段落替换为换行
		content = content.replaceAll("<p .*?>", "\r\n");
		// <br><br/>替换为换行
		content = content.replaceAll("<br\\s*/?>", "\r\n");
		// 去掉其它的<>之间的东西
		content = content.replaceAll("\\<.*?>", "");
		// 还原HTML
		// content = HTMLDecoder.decode(content);
		return content;
	}
}
