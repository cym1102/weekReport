package com.cym.resp;

import com.cym.model.FunctionLog;

public class FunctionLogResp extends FunctionLog {

	String userNames;

	String toStatusName;

	String functionName;

	String mark;
	// 紧急程度 0低级 1中级 2紧急 3特急
	Integer level;

	String levelName;
	// 类型 0：需求 1：bug
	Integer type;


	public String getUserNames() {
		return userNames;
	}

	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}


	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}


	public String getToStatusName() {
		return toStatusName;
	}

	public void setToStatusName(String toStatusName) {
		this.toStatusName = toStatusName;
	}

}
