package com.cym.resp;

import com.cym.model.User;
import com.cym.sqlhelper.config.InitValue;

public class UserResp extends User{

	// 类型名
	String typeName;


	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


}
