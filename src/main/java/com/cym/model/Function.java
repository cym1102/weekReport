package com.cym.model;

import com.cym.sqlhelper.bean.BaseModel;
import com.cym.sqlhelper.config.InitValue;
import com.cym.sqlhelper.config.Table;

//任务
@Table
public class Function extends BaseModel{


	String name;

	String content;

	// 类型 0：需求 1：bug
	@InitValue("0")
	Integer type;

	// 创建人
	String createUserId;

	// 截止时间
	String endTime;

	// 状态 0：等待阶段。1：开发阶段。2：测试阶段。 5验证阶段 3完成阶段. 4否决阶段
	@InitValue("0")
	Integer status;

	// 项目id
	String projectId;

	// 紧急程度 0低级 1中级 2紧急 3特急
	@InitValue("0")
	Integer level;

	// 任务难度 1-10分
	@InitValue("1")
	Integer point;

	// 移动时间
	Long moveTime;
	

	public Long getMoveTime() {
		return moveTime;
	}

	public void setMoveTime(Long moveTime) {
		this.moveTime = moveTime;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

//	public Integer getTestStatus() {
//		return testStatus;
//	}
//
//	public void setTestStatus(Integer testStatus) {
//		this.testStatus = testStatus;
//	}

//	public String getMark() {
//		return mark;
//	}
//
//	public void setMark(String mark) {
//		this.mark = mark;
//	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
