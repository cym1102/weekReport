package com.cym.config;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Filter;
import org.noear.solon.core.handle.FilterChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cym.model.User;
import com.cym.service.UserService;
import com.cym.sqlhelper.bean.Page;

import cn.hutool.core.util.StrUtil;

@Component
public class AppFilter implements Filter {
	Logger logger = LoggerFactory.getLogger(getClass());
	@Inject
	VersionConfig versionConfig;
	@Inject
	UserService userService;

	@Override
	public void doFilter(Context ctx, FilterChain chain) throws Throwable {
		try {
			User user = null;
			if (ctx.path().contains("adminPage") //
					&& !ctx.path().contains("/adminPage/login") //
					&& !ctx.path().endsWith(".js") //
					&& !ctx.path().endsWith(".css") //
					&& !ctx.path().endsWith(".jpg") //
					&& !ctx.path().endsWith(".png") //
			) {
				// 检查登录
				user = (User) ctx.session("user");
				if (user == null) {
					ctx.redirect("/adminPage/login");
					return;
				}

			}

			if (user != null) {
				Long status1count = userService.getStatusCount(user.getId(), 1);
				Long status2count = userService.getStatusCount(user.getId(), 2);

				if (status1count != 0) {
					ctx.attrSet("status1count", status1count);
				}
				if (status2count != 0) {
					ctx.attrSet("status2count", status2count);
				}
			}
			ctx.attrSet("jsrandom", System.currentTimeMillis());
			ctx.attrSet("currentVersion", versionConfig.currentVersion);
			ctx.attrSet("ctx", getCtxStr(ctx));
			ctx.attrSet("page", new Page<>());
			ctx.attrSet("user", ctx.session("user"));
			ctx.attrSet("project", ctx.session("project"));
			
			chain.doFilter(ctx);

		} catch (Throwable e) {
			logger.error(e.getMessage(), e);
		}

	}

	public String getCtxStr(Context context) {
		String httpHost = context.header("X-Forwarded-Host");
		String realPort = context.header("X-Forwarded-Port");
		String host = context.header("Host");

		String ctx = "//";
		if (StrUtil.isNotEmpty(httpHost)) {
			ctx += httpHost;
		} else if (StrUtil.isNotEmpty(host)) {
			ctx += host;
			if (!host.contains(":") && StrUtil.isNotEmpty(realPort)) {
				ctx += ":" + realPort;
			}
		} else {
			host = context.url().split("/")[2];
			ctx += host;
			if (!host.contains(":") && StrUtil.isNotEmpty(realPort)) {
				ctx += ":" + realPort;
			}
		}
		return ctx;

	}
}