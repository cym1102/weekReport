# 项目全流程管理系统


#### 介绍
项目全流程管理系统是一款简易需求与bug管理系统，适合小团队进行快速开发与测试回归，不像禅道那么复杂，仅有敏捷开发最需要的功能。

QQ技术交流群1: 1106758598

QQ技术交流群2: 560797506

邮箱: cym1102@qq.com

微信捐赠二维码

<img src="http://www.nginxwebui.cn/img/weixin.png"  height="200" width="200">

#### 功能说明

项目全流程管理系统是本人因为被禅道巨量的新功能实在烦透了，转而为团队bug与需求管理自研的一款项目管理工具，具有简洁且强大的功能，能够追踪每一个需求与bug的全生命周期，方便团队中的产品经理，开发人员，测试人员进行团队合作开发。

大大简化的操作方式，更利于项目的敏捷迭代，更易于团队成员掌握当前项目需求与bug情况，方便项目经理掌握项目整体进度。

#### 技术说明

本项目是基于solon的java项目, 数据库使用h2, 因此服务器上不需要安装任何数据库, 同时也兼容使用mysql

```
演示地址: http://weekReport.nginxwebui.cn:6020
用户名: admin
密码: admin
```


#### 安装说明

1.安装java运行环境

Ubuntu:

```
apt update
apt install openjdk-11-jdk
```

Centos:

```
yum install java-11-openjdk
yum install subversion
```

Windows:

```
下载JDK安装包 https://www.oracle.com/java/technologies/downloads/
配置JAVA运行环境 
JAVA_HOME : JDK安装目录
Path : JDK安装目录\bin
重启电脑
```


2.下载最新版发行包jar

```
Linux: wget -O /home/weekReport/weekReport.jar http://file.nginxwebui.cn/weekReport-1.0.1.jar

Windows: 直接使用浏览器下载 http://file.nginxwebui.cn/weekReport-1.0.1.jar
```

有新版本只需要修改路径中的版本即可

3.启动程序

```
Linux: nohup java -jar -Dfile.encoding=UTF-8 /home/weekReport/weekReport.jar --server.port=6020 > /dev/null &

Windows: java -jar -Dfile.encoding=UTF-8 D:/home/weekReport/weekReport.jar --server.port=6020
```

参数说明(都是非必填)

--server.port 占用端口, 默认以6020端口启动

--project.home 项目配置文件目录，存放仓库文件, 数据库文件等, 默认为/home/weekReport/

--database.type=mysql 使用其他数据库，不填为使用本地h2数据库

--database.url=jdbc:mysql://ip:port/dbname 数据库url

--database.username=root 数据库用户

--database.password=pass 数据库密码

注意命令最后加一个&号, 表示项目后台运行

#### docker安装说明

本项目制作了docker镜像, 支持 x86_64/arm64 平台

1.安装docker容器环境

Ubuntu:

```
apt install docker.io
```

Centos:

```
yum install docker
```

2.拉取镜像: 

```
docker pull cym1102/weekreport:latest
```

3.启动容器: 

```
docker run -itd -v /home/weekReport:/home/weekReport --privileged=true -p 6020:6020 cym1102/weekreport:latest
```

注意: 

1. 需要映射6020端口

2. 容器需要映射路径/home/weekReport:/home/weekReport, 此路径下存放项目所有数据文件, 包括数据库, 配置文件, 日志等, 升级镜像时, 此目录可保证项目数据不丢失. 请注意备份.


#### 编译说明

使用maven编译打包

```
mvn clean package
```

使用docker构建镜像

```
docker build -t weekreport:latest .
```

#### 添加开机启动


1. 编辑service配置

```
vim /etc/systemd/system/weekreport.service
```

```
[Unit]
Description=weekReport
After=syslog.target
After=network.target
 
[Service]
Type=simple
User=root
Group=root
WorkingDirectory=/home/weekReport
ExecStart=/usr/bin/java -jar /home/weekReport/weekReport.jar
Restart=always
 
[Install]
WantedBy=multi-user.target
```

之后执行

```
systemctl daemon-reload
systemctl enable weekreport.service
systemctl start weekreport.service
```

#### 使用说明

打开 http://ip:6020 进入主页

![输入图片说明](http://www.nginxwebui.cn/img/weekReport/登录.png "login.jpg")

首次打开页面, 初始用户密码为admin/admin, 请及时修改密码. 可在登录界面中选择进入哪个项目, 管理员可进入系统设置项目进行项目及用户的设置.

![输入图片说明](http://www.nginxwebui.cn/img/weekReport/用户管理.png "login.jpg")

登录进入系统设置, 即可进行用户管理, 添加或编辑用户.

![输入图片说明](http://www.nginxwebui.cn/img/weekReport/项目管理.png "admin.jpg")

登录进入系统设置, 即可进行项目管理, 添加或编辑项目.

![输入图片说明](http://www.nginxwebui.cn/img/weekReport/阶段.png "admin.jpg")

登录进入具体项目, 即可查看各阶段的bug与需求, 可在各个阶段中添加或转移bug需求, 实现对bug和需求的全生命周期管理.

![输入图片说明](http://www.nginxwebui.cn/img/weekReport/项目日志.png "admin.jpg")

每一次添加或转移bug需求, 都会留下系统日志, 可以在项目日志处查看项目的相关操作日志.

#### 找回密码

如果忘记了登录密码，可按如下教程找回密码

1.停止weekReport

```
pkill java
```

2.使用找回密码参数运行weekReport.jar

```
java -jar weekReport.jar --project.home=/home/weekReport/ --project.findPass=true
```

--project.home 为项目文件所在目录

--project.findPass 为是否打印用户名密码

运行成功后即可打印出全部用户名密码