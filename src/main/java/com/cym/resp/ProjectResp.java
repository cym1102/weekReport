package com.cym.resp;

import java.util.List;

import com.cym.model.Project;
import com.cym.model.User;

public class ProjectResp extends Project {

	List<User> userList;
	
	String userNames;

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public String getUserNames() {
		return userNames;
	}

	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}

}
