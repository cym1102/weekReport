package com.cym.resp;

import java.util.List;

import com.cym.model.Function;
import com.cym.model.User;

public class FunctionResp extends Function {
	// 创建人
	User createUser;

	List<User> userList;
	String userNames;

	String levelName;

	List<TestLogResp> testLogList;

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public String getUserNames() {
		return userNames;
	}

	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public List<TestLogResp> getTestLogList() {
		return testLogList;
	}

	public void setTestLogList(List<TestLogResp> testLogList) {
		this.testLogList = testLogList;
	}


}
