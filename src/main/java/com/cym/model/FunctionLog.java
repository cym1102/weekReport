package com.cym.model;

import com.cym.sqlhelper.bean.BaseModel;
import com.cym.sqlhelper.config.InitValue;
import com.cym.sqlhelper.config.Table;
@Table
public class FunctionLog extends BaseModel{
	
	String functionId;
	//操作人
	String userId;

	//转移到状态  0：等待阶段。1：开发阶段。2：测试阶段。3：产品确认。4任务完成. 5任务否决
	@InitValue("0")
	Integer toStatus;
	
	// 产生时间
	Long time;
	
	
	

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getToStatus() {
		return toStatus;
	}

	public void setToStatus(Integer toStatus) {
		this.toStatus = toStatus;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	
}
