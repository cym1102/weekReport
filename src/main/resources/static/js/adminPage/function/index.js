$(function() {
	ClassicEditor.create(document.querySelector('#content'), {
		ckfinder: {
			uploadUrl: '/adminPage/function/upload'
		}
	}
	).then(editor => {
		window.editor = editor;
	}).catch(error => {
		console.log(error);
	});

	ClassicEditor.create(document.querySelector('#changeMark'), {
		ckfinder: {
			uploadUrl: '/adminPage/function/upload'
		}
	}
	).then(editor => {
		window.editor2 = editor;
	}).catch(error => {
		console.log(error);
	});


})


function search() {
	$("input[name='pageNum']").val(1);
	$("#searchForm").submit();
}

function add() {
	$("#id").val("");
	$("#name").val("");
	$("input[name='userIds']").prop("checked", false);
	$("input[name='endTime']").val(formatDate(new Date()));
	$("#type option:first").prop("selected", true);
	$("#endTime").val("");
	$("#content").val("");
	$("#point option:first").prop("selected", true);
	$("#level option:first").prop("selected", true);
	$("#mark").val("");
	editor.setData("");

	form.render();
	showWindow("添加任务");
}


function showWindow(title) {
	layer.open({
		type: 1,
		title: title,
		area: ['1000px', '650px'], // 宽高
		content: $('#windowDiv')
	});
}

function addOver() {
	if ($("#name").val() == "") {
		layer.msg("任务名为空");
		return;
	}
	if ($("#addForm input[name='endTime']").val() == "") {
		layer.msg("截止为空");
		return;
	}

	var hasUser = false;
	$("input[name='userIds']").each(function() {
		if ($(this).prop("checked")) {
			hasUser = true;
		}
	});

	if (!hasUser) {
		layer.msg("负责人为空");
		return;
	}


	$("#content").val(editor.getData());
	showLoad();
	$.ajax({
		type: 'POST',
		url: ctx + '/adminPage/function/addOver',
		data: $('#addForm').serialize(),
		dataType: 'json',
		success: function(data) {
			closeLoad();
			if (data.success) {
				location.reload();
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});


}

function edit(id) {
	showLoad();
	$.ajax({
		type: 'GET',
		url: ctx + '/adminPage/function/detail',
		dataType: 'json',
		data: {
			id: id
		},
		success: function(data) {
			closeLoad();
			if (data.success) {
				var func = data.obj;
				$("#id").val(func.id);
				$("#name").val(func.name);
				$("#type").val(func.type);
				$("input[name='endTime']").val(func.endTime);
				$("#content").val(func.content);
				editor.setData(func.content);

				$("input[name='userIds']").prop("checked", false);
				for (let i = 0; i < func.userList.length; i++) {
					var user = func.userList[i];
					$("#users_" + user.id).prop("checked", true);
				}

				$("#level").val(func.level);
				$("#point").val(func.point);
				$("#mark").val(func.mark);

				form.render();
				showWindow("编辑任务");
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});
}

function del(id) {
	if (confirm("确认删除?")) {
		showLoad();
		$.ajax({
			type: 'POST',
			url: ctx + '/adminPage/function/del',
			data: {
				id: id
			},
			dataType: 'json',
			success: function(data) {
				closeLoad();
				if (data.success) {
					location.reload();
				} else {
					layer.msg(data.msg)
				}
			},
			error: function() {
				closeLoad();
				alert("出错了,请联系技术人员!");
			}
		});
	}
}



function move(id) {
	$("#status option:first").prop("selected", true);
	//$("#testStatus1 option:first").prop("selected", true);
	showLoad();
	$.ajax({
		type: 'GET',
		url: ctx + '/adminPage/function/detail',
		dataType: 'json',
		data: {
			id: id
		},
		success: function(data) {
			closeLoad();
			if (data.success) {
				var func = data.obj;
				$("#functionId").val(func.id);
				$("#status").val(func.status);
				//$("#testStatus1").val(func.testStatus);

				form.render();
				layer.open({
					type: 1,
					title: "转移",
					area: ['400px', '400px'], // 宽高
					content: $('#moveDiv')
				});
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});
}

function moveOver() {
	showLoad();
	$.ajax({
		type: 'POST',
		url: ctx + '/adminPage/function/moveOver',
		data: $('#moveForm').serialize(),
		dataType: 'json',
		success: function(data) {
			closeLoad();
			if (data.success) {
				location.reload();
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});

}


function detail(id) {
	showLoad();
	$.ajax({
		type: 'GET',
		url: ctx + '/adminPage/function/detail',
		dataType: 'json',
		data: {
			id: id
		},
		success: function(data) {
			closeLoad();
			if (data.success) {
				var functionResp = data.obj;
				$("#title").html(functionResp.name);
				$("#detail").html(functionResp.content);
				$("#markList").html("");
				for (var i = 0; i < functionResp.testLogList.length; i++) {
					var testLogResp = functionResp.testLogList[i];

					var html = `<div class="status">${testLogResp.descr}</div>`;
					if (testLogResp.testLog.changeMark != null && testLogResp.testLog.changeMark != "") {
						html += `<div class="mark">${testLogResp.testLog.changeMark}</div>`;
					}

					$("#markList").append(html);
				}

				form.render();
				
				$("img").each(function(){
					$(this).attr("id", guid());
				})

				$("img").click(function() {
					window.open(ctx + "/img?url=" + $(this).attr("src"));
				});

				layer.open({
					type: 1,
					title: "详情",
					area: ['1000px', '650px'], // 宽高
					content: $('#detailDiv')
				});
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});

}


function test(id) {
	showLoad();
	$.ajax({
		type: 'GET',
		url: ctx + '/adminPage/function/detail',
		dataType: 'json',
		data: {
			id: id
		},
		success: function(data) {
			closeLoad();
			if (data.success) {
				var func = data.obj;
				$("#functionId2").val(func.id);
				//$("#testStatus2").val(func.testStatus);

				$("input[name='userIds']").prop("checked", false);
				for (let i = 0; i < func.userList.length; i++) {
					var user = func.userList[i];
					$("#users_" + user.id + "_test").prop("checked", true);
				}

				editor2.setData("");

				form.render();
				layer.open({
					type: 1,
					title: "添加日志",
					area: ['1000px', '650px'], // 宽高
					content: $('#testDiv')
				});
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});

}


function testOver() {
	$("#changeMark").val(editor2.getData());
	showLoad();
	$.ajax({
		type: 'POST',
		url: ctx + '/adminPage/function/testOver',
		data: $('#testForm').serialize(),
		dataType: 'json',
		success: function(data) {
			closeLoad();
			if (data.success) {
				location.reload();
			} else {
				layer.msg(data.msg);
			}
		},
		error: function() {
			closeLoad();
			alert("出错了,请联系技术人员!");
		}
	});

}