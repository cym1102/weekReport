package com.cym.controller;

import java.io.File;
import java.util.Date;

import org.noear.solon.annotation.Body;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.ModelAndView;

import com.cym.config.HomeConfig;
import com.cym.utils.BaseController;

import cn.hutool.core.date.DateUtil;

@Controller
@Mapping("")
public class MainController extends BaseController {
	@Inject
	HomeConfig homeConfig;

	@Mapping("")
	public void main(Context ctx) {
		ctx.redirect("/adminPage/login");
	}

	@Mapping("adminPage/file/{month}/{file}")
	public File file(Context ctx, String month, String file) {
		String url = homeConfig.file + "/" + month + "/" + file;
		return new File(url);

	}

	@Mapping("img")
	public ModelAndView img(String url, ModelAndView modelAndView) {
		modelAndView.put("url", url);
		modelAndView.view("/adminPage/login/img.html");
		return modelAndView;
	}
}
